const names = ["Hanna", "Skye", "Kep", "Kefe", "Sami", "Kapa", "Nimmie", "Akkin", "Houd"];
const roles = ["TANK", "DPS", "HEAL", "BENCH"];
const colors = ["#FF0000",  "#FFFF00", "#00FF00", "#0000FF" ]; 
const canvas = document.getElementById("wheelCanvas");
const ctx = canvas.getContext("2d");
const wheelRadius = canvas.height / 2;


const spinSound = new Audio("spin-sound2.mp3");


function startRandomizer() {

  if (names.length === 0) {
    alert("No more names to assign roles.");
    return;
  }

  const numSlices = roles.length;
  const sliceAngle = (2 * Math.PI) / numSlices;
  const startAngle = Math.random() * (2 * Math.PI);

  let spinTime = 0;
  let spinTimeTotal = 4000; // Total spin time in milliseconds
  const spinInterval = 20; // Interval between frame updates in milliseconds

  spinning = true;

  spinSound.play();

  function spinAnimation() {
    spinTime += spinInterval;

    if (spinTime >= spinTimeTotal) {
      stopRandomizer(startAngle);
      return;
    }

    drawWheel(startAngle + easeOut(spinTime / spinTimeTotal) * (2 * Math.PI));
    requestAnimationFrame(spinAnimation);
  }

  requestAnimationFrame(spinAnimation);
}

function stopRandomizer(finalAngle) {
  spinning = false;
  const selectedRoleIndex = Math.floor(finalAngle / ((2 * Math.PI) / roles.length));
  const selectedRole = roles[selectedRoleIndex];

  const bottomRightIndex = (selectedRoleIndex + Math.floor(roles.length / 2)) % roles.length;
  const bottomRightRole = roles[bottomRightIndex];
  
  const selectedNameIndex = Math.floor(Math.random() * names.length);
  const selectedName = names[selectedNameIndex];
  names.splice(selectedNameIndex, 1); // Remove the selected name from the list

  const assignment = {
    name: selectedName,
    role: bottomRightRole,
  };

  
  displayAssignment(assignment); 
}

function drawWheel(rotation) {
  ctx.clearRect(0, 0, canvas.width, canvas.height);

  ctx.strokeStyle = "black";
  ctx.lineWidth = 2;

  roles.forEach((role, index) => {
    const sliceAngle = (2 * Math.PI) / roles.length;
    const angle = rotation + index * sliceAngle;

    ctx.beginPath();
    ctx.arc(wheelRadius, wheelRadius, wheelRadius - 20, angle, angle + sliceAngle);
    ctx.lineTo(wheelRadius, wheelRadius);
    ctx.closePath();

    ctx.fillStyle = colors[index % colors.length];
    ctx.fill();
    ctx.stroke();

    ctx.save();
    ctx.translate(wheelRadius, wheelRadius);
    ctx.rotate(angle + sliceAngle / 2);
    ctx.fillStyle = "black";
    ctx.font = "20px Arial";
   
    ctx.textBaseline = "middle";
    ctx.textAlign = "center";
    const textMargin = wheelRadius * 0.6; 
    ctx.fillText(role, textMargin, 0);
    ctx.restore();
  });
}

function displayAssignment(assignment) {
  const assignmentElement = document.getElementById("assignment");
  assignmentElement.textContent = `${assignment.name} - ${assignment.role}`;
}

function easeOut(t) {
  return 1 - Math.pow(1 - t, 3);
}
